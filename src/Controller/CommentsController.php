<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

#[Route('/comments')]
class CommentsController extends AbstractController
{
    /**
     * @throws \Exception
     */
    #[Route('/new/{post}', name: 'comments_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, Post $post): ?RedirectResponse
    {
        $validator = Validation::createValidator();
        $constraints = new Collection([
            'body' => new NotBlank(),
        ]);

        $body = [
            'body' => $request->get('body'),
        ];

        $violations = $validator->validate($body, $constraints,);

        if ($violations->count()) {
            throw new \Exception($violations[0]->getMessage());
        }
            $comment = new Comment();
            $comment->setBody($request->get('body'));
            $comment->setUser($this->getUser());
            $comment->setPost($post);
            $entityManager->persist($comment);
            $entityManager->flush();
            return $this->redirectToRoute('posts_show', ['id' => $post->getId()]);
    }

    #[Route('/{id}', name: 'comments_delete', methods: ['POST'])]
    public function delete(Request $request, Comment $comment, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('delete', $comment);
        $id = $comment->getPost()->getId();
        if ($this->isCsrfTokenValid('delete' . $comment->getId(), $request->request->get('_token'))) {
            $entityManager->remove($comment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('posts_show', ['id' => $id], Response::HTTP_SEE_OTHER);
    }
}
