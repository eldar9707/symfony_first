<?php

namespace App\Controller;

use App\Entity\Tag;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/tags')]
class TagsController extends AbstractController
{
    #[Route('/new/ajax/{label}', name: 'tags_new', methods: 'POST')]
    public function new(string $label, EntityManagerInterface $entityManager): Response
    {
        $tag = new Tag();
        $tag->setName(trim(strip_tags($label)));
        $entityManager->persist($tag);
        $entityManager->flush();
        $id = $tag->getId();
        return new JsonResponse(['id' => $id]);
    }
}
