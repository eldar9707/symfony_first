<?php

namespace App\DataFixtures;

use App\Factory\CommentFactory;
use App\Factory\FriendshipFactory;
use App\Factory\PostFactory;
use App\Factory\TagFactory;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        UserFactory::createOne(['email' => 'admin@admin.com', 'username' => 'Admin', 'first_name' => 'Admin'])
            ->setRoles(['ROLE_ADMIN']);
        UserFactory::createOne(['email' => 'moder@moder.com', 'username' => 'Moder', 'first_name' => 'Moder'])
            ->setRoles(['ROLE_MODER']);
        UserFactory::createMany(20);
        TagFactory::createMany(30);

        FriendshipFactory::createMany(40, function () {
            return [
                'user' => UserFactory::random(),
                'friend' => UserFactory::random()
            ];
        });
        PostFactory::createMany(60, function () {
            return [
                'user' => UserFactory::random(),
                'tags' => TagFactory::randomRange(0, 5),
            ];
        });
        CommentFactory::createMany(200, function () {
            return [
                'post' => PostFactory::random(['is_approve' => true]),
                'user' => UserFactory::random(),
            ];
        });

        $manager->flush();
    }
}
