<?php

namespace App\Repository;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use function get_class;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function searchByQuery(?string $query): QueryBuilder
    {
        return $this->createQueryBuilder('user')
            ->where('user.first_name LIKE :query')
            ->orWhere('user.last_name LIKE :query')
            ->setParameter('query', '%' . $query . '%');
    }

    public function getUsersQueryBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('user');
    }

    public function getCommentsBelongsUser($user): ArrayCollection
    {
        $em = $this->_em;
        $rsm = new ResultSetMappingBuilder($em, ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addRootEntityFromClassMetadata(Comment::class, 'c');
        $rsm->addJoinedEntityFromClassMetadata(Post::class, 'p', 'c', 'post', ['id' => 'post_id']);
        $query = $em->createNativeQuery(
            "
            SELECT {$rsm->generateSelectClause()}
                    FROM comment c
                    INNER JOIN post p ON c.post_id = p.id
                    WHERE c.user_id = :user_id
                    GROUP BY c.post_id
            ", $rsm)
            ->setParameter('user_id', $user)
            ->getResult();
        return new ArrayCollection($query);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getPostsJoinedToUser($user)
    {
        $query = $this->_em->createQuery(
            'SELECT u, p, pc, t
            FROM App\Entity\User u
            LEFT JOIN u.posts p
            LEFT JOIN p.comments pc
            LEFT JOIN p.tags t
            WHERE u.id = :id'
        )->setParameter('id', $user);
        return $query->getOneOrNullResult();
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
