<?php

namespace App\Security;

use App\Entity\Post;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PostVoter extends Voter
{
    const EDIT = 'edit';
    const DELETE = 'delete';

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::EDIT, self::DELETE])) {
            return false;
        }

        if (!$subject instanceof Post) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        $post = $subject;
        switch ($attribute) {
            case self::EDIT:
                return $this->canDelete($post, $user);
            case self::DELETE:
                return $this->canEdit($post, $user);
        }
        throw new \LogicException('This code should not be reached!');
    }

    private function canEdit(Post $post, User $user): bool
    {
        if ($this->canDelete($post, $user)) {
            return true;
        }
        return false;
    }

    private function canDelete(Post $post, User $user): bool
    {
        return $user === $post->getUser();
    }
}