<?php

namespace App\Controller\Admin;

use App\Entity\Image;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UserCrudController extends AbstractCrudController
{
    private UserPasswordHasherInterface $passwordEncoder;

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setSearchFields(['username', 'first_name', 'last_name', 'email'])
            ->setEntityLabelInPlural('Users')
            ->showEntityActionsInlined()
            ->setEntityPermission('ROLE_ADMIN');
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            ImageField::new('avatar.imageName', 'Avatar')
                ->setBasePath('users/images/')
                ->setTemplatePath('admin/field/avatar.html.twig')
                ->hideOnForm(),
//            ImageField::new('avatar.imageFile')
//                ->onlyWhenUpdating()
//                ->setFormType(VichImageType::class)
//           -> setUploadDir('public'),
//                ->setTemplatePath('admin/field/avatar.html.twig'),
//            ImageField::new('avatar.imageFile')
//                ->setBasePath('users/images/')
//                ->setUploadDir('public/users/images/')
//            ImageField::new('avatar.imageFile')
//                ->setBasePath('users/images')
//                ->setUploadDir('public/users/images')
//                ->setUploadedFileNamePattern('[randomhash],[extension]')
//                ->setRequired(false)
//                ->onlyWhenUpdating(),
//            ImageField::new('avatar.imageName', 'Image')->setUploadDir('public/users/images'),
            EmailField::new('email'),
            Field::new('password', 'Password')
                ->onlyOnForms()
                ->setFormType(PasswordType::class),
            TextField::new('username')
                ->setCssClass('text-dark'),
            TextField::new('gender')
                ->formatValue(function ($e) {
                    return $e == 'm' ? 'Male' : 'Female';
                })
                ->setFormType(ChoiceType::class)->setFormTypeOptions([
                    'choices' => [
                        'Male' => 'm',
                        'Female' => 'f'
                    ],
                ]),
            DateField::new('birthday'),
            TextField::new('first_name')
                ->hideOnIndex(),
            TextField::new('last_name')
                ->hideOnIndex(),
            ArrayField::new('roles')
                ->hideOnIndex(),
//            CollectionField::new('avatar')->setTemplatePath('admin/field/children.html.twig'),
//            TextareaField::new('images')->setFormType(VichImageType::class)
//            AssociationField::new('avatar')->autocomplete()
//            VichImageField::new('avatar.imageName'),
//            VichImageField::new('images')->hideOnForm(),


//            ImageField::new('avatar', 'Image')
//                ->onlyOnIndex()
//                ->setBasePath('/users/images'),
        ];
    }

    public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createEditFormBuilder($entityDto, $formOptions, $context);
        $this->addEncodePasswordEventListener($formBuilder);
        return $formBuilder;
    }

    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
        $this->addEncodePasswordEventListener($formBuilder);
        return $formBuilder;
    }

    /**
     * @required
     */
    public function setEncoder(UserPasswordHasherInterface $passwordEncoder): void
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function addEncodePasswordEventListener(FormBuilderInterface $formBuilder)
    {
        $formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            /** @var User $user */
            $user = $event->getData();
            if ($user->getPassword()) {
                $user->setPassword($this->passwordEncoder->hashPassword($user, $user->getPassword()));
            }
        });
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }

    public function createEditForm(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormInterface
    {
//        dd($entityDto);
        /** @var User $user */
        $user = $entityDto->getInstance();
        $image = new Image();
        $image->setUser($user);
        $user->setAvatar($image);
        return $this->createEditFormBuilder($entityDto, $formOptions, $context)->getForm();
    }

//    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
//    {
//        /** @var User $user */
//        $user = $entityInstance;
////        /** @var User $user */
////        $user = $entityInstance;
//////        dd($user->getAvatar());
////
////        if (!$user->getAvatar()) {
////            $avatar = $user->getAvatar();
////            $user->setAvatar($avatar);
////            $entityManager->persist($user);
////            $entityManager->flush();
////            return $this->redirectToRoute('profile');
////        }
////        /** @var User $user */
////        $user = $entityDto->getInstance();
//        $image = new Image();
//        $image->setUser($user);
//        $user->setAvatar($image);
//    }
}
