<?php

namespace App\Controller;

use App\Entity\Image;
use App\Form\ImageFormType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    #[Route('/home/page', name: 'home_page')]
    #[IsGranted('ROLE_USER')]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(ImageFormType::class);
        $form->handleRequest($request);
        $image = new Image();
//        if ($form->isSubmitted() && $form->isValid()) {
//            $image->setImageFile($form->get('imageFile')->getData());
//            $user->addImage($image);
//            $entityManager->persist($user);
//            $entityManager->flush();
//            $user->setPassword(
//                $userPasswordHasher->hashPassword(
//                    $user,
//                    $form->get('plainPassword')->getData()
//                )
//            );
//
//            $entityManager->persist($user);
//            $entityManager->flush();
//            // do anything else you need here, like send an email
//
//            return $userAuthenticator->authenticateUser(
//                $user,
//                $authenticator,
//                $request
//            );
//        }

        return $this->render('home_page/index.html.twig', [
            'imageForm' => $form->createView(),
        ]);
    }
}
