<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PostCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Post::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setSearchFields(['title', 'text', 'user.username'])
            ->setEntityLabelInPlural('Posts')
            ->showEntityActionsInlined()
            ->setEntityPermission('ROLE_MODER')
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title'),
            TextField::new('user.username', 'Author')
                ->hideOnForm(),
            BooleanField::new('is_approve', 'Status'),
            TextEditorField::new('text')
                ->hideOnIndex(),
            ArrayField::new('tags', 'Tags')
                ->onlyOnDetail(),
            AssociationField::new('tags')
                ->onlyOnForms(),
            ArrayField::new('comments')
                ->onlyOnDetail()
                ->setTemplatePath('admin/field/comments.html.twig')
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->setPermissions([
                Action::DELETE => 'ROLE_ADMIN',
                Action::NEW => 'ROLE_ADMIN',
            ]);
    }

    public function createEntity(string $entityFqcn): Post
    {
        /** @var User $user */
        $user = $this->getUser();
        $post = new Post();
        $post->setUser($user);

        return $post;
    }
}
