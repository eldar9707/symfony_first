<?php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\User;
use App\Form\ImageFormType;
use App\Repository\FriendshipRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
class UserProfileController extends AbstractController
{
    /**
     * @throws NonUniqueResultException
     */
    #[Route('/profile/show/{id}', name: 'show_profile')]
    public function showProfile(User $user, FriendshipRepository $friendshipRepository, UserRepository $userRepository): Response
    {
        $comments = $userRepository->getCommentsBelongsUser($user);
        $user = $userRepository->getPostsJoinedToUser($user);
        if ($user === $this->getUser()) {
            return $this->redirectToRoute('profile');
        }
        $userFriends = $user->getFriends();

        $hasMyFriendshipRequest = $friendshipRepository->findOneBy([
            'user' => $this->getUser(),
            'friend' => $user,
            'status' => false
        ]);

        $hasFriendshipRequest = $friendshipRepository->findOneBy([
            'user' => $user,
            'friend' => $this->getUser(),
            'status' => false
        ]);


        $isFriend = $friendshipRepository->findOneBy([
            'user' => $this->getUser(),
            'friend' => $user,
            'status' => true
        ]);

        return $this->render('profile/index.html.twig', [
            'user' => $user,
            'userFriends' => $userFriends,
            'hasMyFriendshipRequest' => $hasMyFriendshipRequest,
            'hasFriendshipRequest' => $hasFriendshipRequest,
            'isFriend' => $isFriend,
            'comments' => $comments
        ]);
    }

    /**
     * @throws NonUniqueResultException
     */
    #[Route('/profile', name: 'profile')]
    public function setImage(Request $request, EntityManagerInterface $entityManager, UserRepository $userRepository): Response
    {
        $user = $userRepository->getPostsJoinedToUser($this->getUser()->getId());
        $comments = $userRepository->getCommentsBelongsUser($user);

        $image = new Image();
        $form = $this->createForm(ImageFormType::class, $image);
        $form->handleRequest($request);
        $userFriends = $user->getFriends();

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form->getData();
            $entityManager->persist($image);
            $user->addImage($image);
            if (!is_null($request->get('avatar1'))) {
                $user->setAvatar($image);
            }

            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('profile');
        }
        if (!is_null($request->get('avatar'))) {
            $avatar = $entityManager->getRepository(Image::class)->find($request->get('avatar'));
            $user->setAvatar($avatar);
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('profile');
        }

        return $this->render('profile/index.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            'userFriends' => $userFriends,
            'comments' => $comments
        ]);
    }

    /**
     * @Route("/profile{id}", name="delete_img")
     */
    public function deleteImg(Image $image, EntityManagerInterface $entityManager): RedirectResponse
    {
        if ($image->getUser() !== $this->getUser()) {
            throw $this->createAccessDeniedException();
        }
        $projectDir = $this->getParameter('kernel.project_dir');
        $filename = $image->getImageName();
        $fileSystem = new Filesystem();
        $fileSystem->remove($projectDir . '/public/uploads/' . $filename);
        $entityManager->remove($image);
        $entityManager->flush();

        return $this->redirectToRoute('profile');
    }
}
