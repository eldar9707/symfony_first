<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
//        return parent::index();
        $routeBuilder = $this->get(AdminUrlGenerator::class);
        return $this->redirect($routeBuilder->setController(PostCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Admin Panel');
    }

    public function configureMenuItems(): iterable
    {
            yield MenuItem::linkToDashboard('Home', 'fa fa-home');
            yield MenuItem::linkToRoute('Client Side', 'fab fa-mandalorian', 'home_page');
            if ($this->isGranted('ROLE_MODER') or $this->isGranted('ROLE_ADMIN')) {
                yield MenuItem::section('Posts');
                yield MenuItem::linkToCrud('Posts', 'fas fa-newspaper', Post::class);
            }
            yield MenuItem::section('Users')
                ->setPermission('ROLE_ADMIN');
            yield MenuItem::linkToCrud('Users', 'fa fa-user', User::class)
                ->setPermission('ROLE_ADMIN');
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        return parent::configureUserMenu($user)
            ->setName($user->getUsername())
            ->setGravatarEmail($user->getEmail())
            ->addMenuItems([
                MenuItem::linkToRoute('My Profile', 'fa fa-id-card', 'profile'),
            ]);
    }
}
