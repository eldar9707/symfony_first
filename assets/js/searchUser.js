
$(document).ready(function () {
    let search = $("#selUser");
    search.select2({
        minimumInputLength: 2,
        placeholder: 'Search...',
        ajax: {
            url: "ajax/search-user",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    search: params.term
                };
            },
            processResults: function (response) {
                return {
                    results: response.data
                };
            },
            cache: true,
            success: function (data) {
                $('#users-index').html(data.view)
            }
        }
    });
    search.on("select2:selecting", function (e) {
        window.location.href = '/profile/show/' + e.params.args.data.id;
    });
});