<?php

namespace App\Controller;

use App\Entity\Friendship;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/users')]
#[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
class UsersController extends AbstractController
{
    #[Route('/', name: 'users_index', methods: ['GET', 'POST'])]
    public function index(UserRepository $userRepository, Request $request): Response
    {
        $queryBuilder = $userRepository->getUsersQueryBuilder();
        $pagerfanta = new Pagerfanta(new QueryAdapter($queryBuilder));
        $pagerfanta->setMaxPerPage(4);
        $pagerfanta->setCurrentPage($request->query->get('page', 1));
        return $this->render('users/index.html.twig', [
            'pager' => $pagerfanta,
        ]);
    }

    #[Route('/request/{id}', name: 'send_friendship_request', methods: ['GET'])]
    public function sendFriendshipRequest(User $user, EntityManagerInterface $entityManager): RedirectResponse
    {
        $this->denyAccessUnlessGranted('add', $user);
        $this->getUser()->addFriend($user);
        $entityManager->persist($this->getUser());
        $entityManager->flush();
        return $this->redirectToRoute('show_profile', ['id' => $user->getId()]);
    }

    #[Route('/confirm/{id}', name: 'confirm_friendship', methods: ['GET'])]
    public function confirmFriendship(EntityManagerInterface $entityManager, Friendship $friendship): RedirectResponse
    {
        $fs = new Friendship();
        $fs->setUser($friendship->getFriend());
        $fs->setFriend($friendship->getUser());
        $fs->setStatus(true);
        $friendship->getUser()->addFriendship($fs);
        $friendship->setStatus(true);
        $entityManager->persist($friendship);
        $entityManager->persist($fs);
        $entityManager->flush();

        return $this->redirectToRoute('show_profile', ['id' => $friendship->getUser()->getId()]);
    }

    #[Route('/friendship_list', name: 'requests', methods: ['GET'])]
    public function friendshipRequestList(LoggerInterface $logger): Response
    {
        $logger->info('Inside the controller!');
        $friendRequests = $this->getUser()->getFriendsWithMe();
        return $this->render('friends/friend_requests.html.twig', [
            'friendRequests' => $friendRequests,
        ]);
    }

    #[Route('/ajax/search-user', name: 'ajax_search_user', methods: ['GET', 'POST'])]
    public function searchUser(Request $request, UserRepository $userRepository): Response
    {
        $query = $request->get('search');
        $data = [];
        $users = $userRepository->searchByQuery($query)->getQuery()->getResult();
        foreach ($users as $value) {
            $data[] = ['id' => $value->getId(), 'text' => $value->getFirstName() . " " . $value->getLastName()];
        }
        $view = $this->renderView('users/_users_index.html.twig', [
            'pager' => $users
        ]);

        return new JsonResponse(['data' => $data, 'view' => $view]);
    }
}
