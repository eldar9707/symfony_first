<?php

namespace App\Security;

use App\Entity\Post;
use App\Entity\User;
use App\Repository\FriendshipRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{
    const ADD = 'add';

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::ADD])) {
            return false;
        }

        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $authUser = $token->getUser();

        if (!$authUser instanceof User) {
            return false;
        }

        $user = $subject;
        switch ($attribute) {
            case self::ADD:
                return $this->canAdd($user, $authUser);
        }
        throw new \LogicException('This code should not be reached!');
    }

    private function canAdd(User $user, User $authUser): bool
    {
        return $user !== $authUser;
    }
}